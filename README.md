# Les Petits Monstres de France : Adil BENAMEUR / Baptiste HERBECQ
[![pipeline status](https://gitlab.univ-lille.fr/adil.benameur.etu/les-petits-monstres-de-france/badges/master/pipeline.svg)](https://gitlab.univ-lille.fr/adil.benameur.etu/les-petits-monstres-de-france/commits/master)

![Les Petits Monstres de France](https://img.shields.io/website?down_message=DOWN&up_message=UP&url=http%3A%2F%2Flespetitsmonstresdefrance.tk%2F)


## Concept
Un petit héros défend la France des petits monstres qui l’envahissent. Pour cela il doit résoudre des opérations mathématiques, des problèmes logiques, traduire des mots de l’anglais vers le français et l’inverse ou bien restituer des connaissances historiques. A ces modules sont associés des couleurs (voir ci-dessous).

## Gameplay
Le joueur commence sur la carte de France (pas encore implémenté), et devra choisir quel monstre éloigner. Chaque monstre est différent : il souffre plus selon sa couleur de bonne réponse provenant de la matière associée à cette couleur. Par exemple, les mathématiques étant associés au jaune, le joueur fera plus de dégâts à un monstre jaune s’il répond correctement à une question portant sur les mathématiques.
Une fois le monstre choisi, il devra attaquer le monstre en répondant correctement à plusieurs questions portant sur différentes matières : les mathématiques, l’histoire, la logique et l’anglais.
Si le joueur ne répond pas correctement, le monstre regagne de la vie. Le joueur à perdu si le monstre est devenu trop fort pour lui (passé un certain niveau et / ou points de vie)

## Ce que nous avons réalisé jusqu'ici
 - le système de combat de monstre
 - la gestion des questions avec un fichier csv
 - la création d'un language interprété pour varier nos questions
 - la création des designs des monstres et leur affichage en couleur

## A noter
Pour profiter des couleur RGB ANSI, vous pouvez installer un [terminal compatible](https://gist.github.com/XVilka/8346728#terminals--true-color).

## Organisation du dêpot
### Dossiers
- src contient les sources du projet
- lib contient la librairie ap.jar
- shots contient des captures d'écran du projet
- html_doc contient la javadoc du projet
- design_graphique contient les images, les fichiers textes et le programme python permettant de convertir les images en ASCII

### Fichiers
- compile.sh permet de compile le projet (les classes sont stockées dans un dossier classes ignoré par git)
- run.sh permet de lancer le projet
- gitlab-ci.yml est le fichier relatif au déploiment continu du projet à l'adresse [lespetitsmonstresdefrance.tk](http://lespetitsmonstresdefrance.tk)
- .gitignore contient les fichiers et dossier à ne pas inclure dans le dêpot


