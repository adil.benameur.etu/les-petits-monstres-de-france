#!/bin/bash

git rm -rf --quiet --cached .
git add .

if [[ $(git status --short) ]]; then
    echo "Les modifications suivantes ont été apportées :"
    echo -e "$(git status --short)"
    git commit --quiet -m "$(echo -e "$(date "+%d-%m-%Y %H:%M")\n$(git status --short)")"
    (git push --quiet && echo -e "Déploiement réussi ! \xF0\x9F\x9A\x80") || echo -e "Le déploiement a échoué \xE2\x9D\x8C"
else
    echo "Aucune modification n'a été faite"
fi
