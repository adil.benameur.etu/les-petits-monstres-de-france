#!/bin/bash


if echo $1 | grep -xqe "-r"
then
  echo "Réinitialisation de la sauvegarde"
  cp -r ressources/* classes/
fi

cd classes
export CLASSPATH=`find ../lib -name "*.jar" | tr '\n' ':'`

java -cp ${CLASSPATH}:. Main

cd ..
