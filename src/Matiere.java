/**
 * Class permettant la gestion des différentes matières
 *
 * @see Question
 */

class Matiere {
    String MATHS = "1"; // bleu
    String LOGIQUE = "2"; // violet
    String ANGLAIS = "3"; // rouge
    String HISTOIRE_GEO = "4"; // vert
    String[] CONV = {"aux Mathématiques", "à la Logique", "à l'Anglais", "à l'Histoire"};
}
