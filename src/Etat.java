/**
 * Etat est la class décrivant les possibles états d'un monstre
 * Le numero des états est relatif à l'id des design des monstre dans le fichier Monstres.csv
 */

class Etat {
    int NORMAL = 0;
    int REGEN = 1;
    int ATTAQUE = 2;
}
