/**
 * La class Monstre définie l'objet monstre avec :
 *      - son niveau (par défault : 1)
 *      - ses points de vie (par défault : 2000)
 *      - sa matière point faible (sa couleur) comprise entre 1 et 4 en fonction du type de la matière (design stocké dans Monstre.csv)
 *          - maths = bleu
 *          - logique = violet
 *          - anglais = rouge
 *          - histoire = vert
 *
 * @see Matiere
 * @see Etat
 */

class Monstre {
    int niveau = 0;
    int pointDeVie = 0;
    String matierePointFaible = "0";

    // Convertion de la matière point faible
    String[] convertionNom = {"", "Monstre bleu", "Monstre violet", "Monstre rouge", "Monstre vert"};
}