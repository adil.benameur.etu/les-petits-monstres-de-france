import extensions.CSVFile;

class Main extends Program {
    // Initialisation des contantes
    final CSVFile fichierQuestion = loadCSV("Questions.csv", '\t');
    final String[][] monstres = new Ascii().monstres;
    final Matiere MATIERE = new Matiere();
    final Etat ETAT_MONSTRE = new Etat();


    //=====================================================================================================================================================================================
    //                                                                  FONCTION AFFICHAGE
    //=====================================================================================================================================================================================


    /**
     * afficherMenu affiche le menu avant le lancement de la partie
     **/

    void afficherMenu() {
        println("===== Les petits monstres de France =====\n");
        println(new Ascii().monstres[1][0]);
        println("====================== Menu ============================");
        println("1 - Jouer");
        println("2 - Satistique");
        println("3 - Quitter");
        println("========================================================");
    }

    /**
     * afficherStatistique affiche les stats
     *
     * @param difficulte
     * @param nbJour
     * @param monstresTues
     * @param pseudo
     */
    void afficherStatistique(Difficulte difficulte, int nbJour, int monstresTues, String pseudo) {
        clearScreen();

        println("=================== Statistique ========================");
        println("Pseudo du joueur : " + pseudo);
        if (monstresTues > 1) {
            println("Nombre de monstres éduqués : " + monstresTues);
        } else {
            println("Nombre de monstre éduqué : " + monstresTues);
        }

        if (nbJour > 1) {
            println("Nombre de jours : " + nbJour);
        } else {
            println("Nombre de jour : " + nbJour);
        }

        println();
        println("Niveau en mathématiques : " + difficulte.math.niveauGlobal);
        println("Niveau en logique : " + difficulte.logique.niveauGlobal);
        println("Niveau en anglais : " + difficulte.anglais.niveauGlobal);
        println("Niveau en histoire : " + difficulte.histoire.niveauGlobal);
        println();
        println("========================================================");

    }

    /**
     * afficherLeMonstreCombat affiche le monstre lorsqu'il est en combat
     *
     * @param monstre
     * @param etatMonstre
     */

    void afficherLeMonstreCombat(Monstre monstre, int etatMonstre) {
        String aAffiche = new Ascii().monstres[stringToInt(monstre.matierePointFaible)][etatMonstre];
        int i = 0;

        while (i < length(aAffiche)) {
            if (charAt(aAffiche, i) == '\\') {
                println();
                i++;
            } else {
                print(charAt(aAffiche, i));
            }
            i++;
        }
        println("\u001B[38;2;250;0;0m\u2764\u001B[0m  " + monstre.pointDeVie);
    }

    /**
     * afficherCombat affiche la demande de choix à l'utilisateur
     *
     * @param monstre
     * @param etat
     */

    void afficherCombat(Monstre monstre, int etat) {
        clearScreen();
        afficherLeMonstreCombat(monstre, 0);
        println("Avec quoi va-t-on attaquer le monstre ?");
        println("========================================================");
        println("1. Mathématiques                 2. Logique");
        println();
        println("3. Anglais                       4. Histoire");
        println("PS: ce monstre est sensible " + MATIERE.CONV[stringToInt(monstre.matierePointFaible) - 1] + " !");
        println("========================================================");
    }

    /**
     * afficherQuestion affiche la question au joueur
     *
     * @param question est l'objet contenant la question
     * @param monstre
     * @param etat
     */

    void afficherQuestion(Question question, Monstre monstre, int etat) {
        clearScreen();
        afficherLeMonstreCombat(monstre, etat);
        println("Quel est le resultat ?");
        println("========================================================");
        println();
        println(question.question);
        println();
        println("========================================================");
    }

    /**
     * afficherReponse affiche la réponse à afficher d'une question + Faux ou Vrai
     *
     * @param question    la question contenant la réponse
     * @param reponseVrai la réponse du joueur
     * @param monstre
     * @param etat
     */
    void afficherReponse(Question question, boolean reponseVrai, Monstre monstre, int etat) {
        clearScreen();
        afficherLeMonstreCombat(monstre, etat);
        println("========================================================");
        println();
        if (reponseVrai) {
            print("Vrai ! ");
            println("la réponse était : " + question.reponseAffiche);
        } else {
            print("Faux ! ");
            println("la réponse était : " + question.reponseAffiche);
        }
        println();
        println("========================================================");
        readString();
    }

    /**
     * afficherCarte affiche la carte, avec les monstres, le nb de tour et la touche pour quitter
     *
     * @param nbJour
     * @param monstresSurLaCarte
     */

    void afficherCarte(int nbJour, Monstre[] monstresSurLaCarte) {
        String[] conv = new Monstre().convertionNom;
        Ascii ascii = new Ascii();
        boolean whiteBG = true;


        String b = "\u001B[48;2;255;255;255m  \u001B[0m";
        clearScreen();

        //ligne 1
        println("              " + b + b + "                  " + "1 - Lille" + " | " +
                (monstresSurLaCarte[0].niveau == 0 ? "Il n'y a pas de monstre dans notre ville." : conv[stringToInt(monstresSurLaCarte[0].matierePointFaible)] + ", niveau " + monstresSurLaCarte[0].niveau));
        //ligne 2
        println("            " + b + b +
                colorString("1", monstresSurLaCarte[0].niveau == 0 ? 0 : stringToInt(monstresSurLaCarte[0].matierePointFaible), whiteBG) + b + b + b + "              " + "");
        //ligne 3
        println("      " + b + "  " + b + b + b + b + b + b + b + b + "          " + "2 - Paris" + " | " +
                (monstresSurLaCarte[1].niveau == 0 ? "Il n'y a pas de monstre dans notre ville." : conv[stringToInt(monstresSurLaCarte[1].matierePointFaible)] + ", niveau " + monstresSurLaCarte[1].niveau));
        //ligne 4
        println("  " + b + b + b + b + b + b + b + colorString("2", monstresSurLaCarte[1].niveau == 0 ? 0 : stringToInt(monstresSurLaCarte[1].matierePointFaible), whiteBG) + b + b + b + b + b + b + b + b + "    " + "");
        //ligne 5
        println(b + b + b + colorString("3", monstresSurLaCarte[2].niveau == 0 ? 0 : stringToInt(monstresSurLaCarte[2].matierePointFaible), whiteBG) + b + b + b + b + b + b + b + b + b + b + b + colorString("4", monstresSurLaCarte[3].niveau == 0 ? 0 : stringToInt(monstresSurLaCarte[3].matierePointFaible), whiteBG) + b + b + "    " + "3 - Rennes" + " | " +
                (monstresSurLaCarte[2].niveau == 0 ? "Il n'y a pas de monstre dans notre ville." : conv[stringToInt(monstresSurLaCarte[2].matierePointFaible)] + ", niveau " + monstresSurLaCarte[2].niveau));
        //ligne 6
        println("  " + b + b + b + b + b + b + b + b + b + b + b + b + b + b + "      " + "");
        //ligne 7
        println("    " + b + b + b + b + b + b + b + b + b + b + b + b + "        " + "4 - Strasbourg" + " | " +
                (monstresSurLaCarte[3].niveau == 0 ? "Il n'y a pas de monstre dans notre ville." : conv[stringToInt(monstresSurLaCarte[3].matierePointFaible)] + ", niveau " + monstresSurLaCarte[3].niveau));
        //ligne 8
        println("      " + b + b + b + b + b + b + b + b + b + b + "          " + "");
        //ligne 9
        println("      " + b + b + b + b + b + b + b + b + b + b + b + "        " + "5 - Lyon" + " | " +
                (monstresSurLaCarte[4].niveau == 0 ? "Il n'y a pas de monstre dans notre ville." : conv[stringToInt(monstresSurLaCarte[4].matierePointFaible)] + ", niveau " + monstresSurLaCarte[4].niveau));
        //ligne 10
        println("        " + b + b + b + b + b + b + b + colorString("5", monstresSurLaCarte[4].niveau == 0 ? 0 : stringToInt(monstresSurLaCarte[4].matierePointFaible), whiteBG) + b + b + b + "        " + "");
        //ligne 11
        println("        " + b + colorString("6", monstresSurLaCarte[5].niveau == 0 ? 0 : stringToInt(monstresSurLaCarte[5].matierePointFaible), whiteBG) + b + b + b + b + b + b + b + b + b + "        " + "6 - Bordeaux" + " | " +
                (monstresSurLaCarte[5].niveau == 0 ? "Il n'y a pas de monstre dans notre ville." : conv[stringToInt(monstresSurLaCarte[5].matierePointFaible)] + ", niveau " + monstresSurLaCarte[5].niveau));
        //ligne 12
        println("      " + b + b + b + b + b + b + b + b + b + b + b + b + "      " + "");
        //ligne 13
        println("      " + b + b + b + b + b + b + b + b + b + b + b + b + "      " + "7 - Toulouse" + " | " +
                (monstresSurLaCarte[6].niveau == 0 ? "Il n'y a pas de monstre dans notre ville." : conv[stringToInt(monstresSurLaCarte[6].matierePointFaible)] + ", niveau " + monstresSurLaCarte[6].niveau));
        //ligne 14
        println("    " + b + b + b + b + b + colorString("7", monstresSurLaCarte[6].niveau == 0 ? 0 : stringToInt(monstresSurLaCarte[6].matierePointFaible), whiteBG) + b + b + b + b + colorString("8", monstresSurLaCarte[7].niveau == 0 ? 0 : stringToInt(monstresSurLaCarte[7].matierePointFaible), whiteBG) + b + b + b + "        " + "");
        //linge 15
        println("          " + b + b + b + b + b + "  " + b + b + "          " + "8 - Marseille" + " | " +
                (monstresSurLaCarte[7].niveau == 0 ? "Il n'y a pas de monstre dans notre ville." : conv[stringToInt(monstresSurLaCarte[7].matierePointFaible)] + ", niveau " + monstresSurLaCarte[7].niveau));
        //ligne 16
        println();
        //ligne 17
        println("              Jour n° " + nbJour);
    }

    /**
     * afficherPersonnagePrenom affiche le personnage et demande le pseudo
     */

    void afficherPersonnagePrenom() {

        println(new Ascii().personnage);
        println();
        println("====================================\n");
        println("Présente-toi vaillant combattant : \n");
        println("====================================");
    }


    //=====================================================================================================================================================================================
    //                                                                   
    //=====================================================================================================================================================================================


    /**
     * matiereChoisie force le joueur à choisir une matière valide
     *
     * @return la matière choisie (MATIERE)
     * @see Matiere
     */

    String matiereChoisie() {
        String numeroMatiere = "";
        boolean estValide = false;
        while (!estValide) {
            numeroMatiere = readString();


            switch (numeroMatiere) {
                case "1":
                    return MATIERE.MATHS;
                case "2":
                    return MATIERE.LOGIQUE;
                case "3":
                    return MATIERE.ANGLAIS;
                case "4":
                    return MATIERE.HISTOIRE_GEO;
                default:

            }
        }
        return null;
    }


    /**
     * verifier compare la reponse du joueur avec celle de l'objet question passé en paramètre
     *
     * @param question est l'objet content la bonne réponse
     * @param reponse  est la réponse de l'utilisateur
     * @return question.reponse == reponse
     */

    boolean reponseBonne(Question question, String reponse) {
        return equals(question.reponse, replace(reponse, ".", ",", 1));
    }

    void testReponseBonne() {
        Question testQuestion = new Question();
        testQuestion.reponse = "bonjour";

        assertTrue(reponseBonne(testQuestion, "bonjour"));
        assertFalse(reponseBonne(testQuestion, "nan nan nan"));
    }

    /**
     * pointFaible vérifie si le joueur attaque avec le point faible du monstre
     *
     * @param question contient le type de la question posée au joueur
     * @param monstre  contient la matière point faible du monstre
     * @return question.type == monstre.matierePointFaible
     */

    boolean pointFaible(Question question, Monstre monstre) {
        return equals(question.type, monstre.matierePointFaible);
    }

    void testPointFaible() {
        Question testQuestion = new Question();
        Monstre testMonstre = new Monstre();
        testQuestion.type = "MATH";

        testMonstre.matierePointFaible = "MATH";
        assertTrue(pointFaible(testQuestion, testMonstre));

        testMonstre.matierePointFaible = "HISTOIRE";
        assertFalse(pointFaible(testQuestion, testMonstre));
    }

    /**
     * mettreAJourMonstre met à jour le monstre après l'attaque du joueur
     *
     * @param question
     * @param monstre
     * @param estVrai
     */
    void mettreAJourMonstre(Question question, Monstre monstre, boolean estVrai) {
        if (estVrai) {
            if (pointFaible(question, monstre)) {
                monstre.pointDeVie = monstre.pointDeVie - 1000;
            } else {
                monstre.pointDeVie = monstre.pointDeVie - 500;
            }
        } else {
            monstre.pointDeVie = monstre.pointDeVie + 100;
        }
    }

    void testMettreAJourMonstre() {

        Question testQuestion = new Question();
        Monstre testMonstre1 = new Monstre();
        testQuestion.type = "MATH";
        testMonstre1.matierePointFaible = "MATH";
        testMonstre1.pointDeVie = 2000;

        mettreAJourMonstre(testQuestion, testMonstre1, true);
        assertEquals(testMonstre1.pointDeVie, 1000);

        mettreAJourMonstre(testQuestion, testMonstre1, false);
        assertEquals(testMonstre1.pointDeVie, 1100);

        testMonstre1.matierePointFaible = "HISTOIRE";
        mettreAJourMonstre(testQuestion, testMonstre1, true);
        assertEquals(testMonstre1.pointDeVie, 600);

    }

    /**
     * monstreEstVivant vérifie les points de vie du monstre en paramètre.
     * return false si monstre.pointDeVie > 0
     *
     * @param monstre : le monstre ayant les points de vie à vérifier
     * @return monstre.pointDeVie > 0
     */

    boolean monstreEstVivant(Monstre monstre) {
        return (monstre.pointDeVie > 0);
    }

    void testMonstreEstVivant() {
        Monstre testMonstre = new Monstre();
        testMonstre.pointDeVie = 2000;
        assertTrue(monstreEstVivant(testMonstre));

        testMonstre.pointDeVie = 0;
        assertFalse(monstreEstVivant(testMonstre));
    }


    void testMonterDifficulteGlobalReponseBonne() {
        Difficulte difficulte = new Difficulte();
        Difficulte tests = new Difficulte();
        Question question = new Question();

        question.type = "1";
        question.genreQuestion = "multiplication";

        monterDifficulteGlobalReponseBonne(difficulte, true, question);
        assertEquals(difficulte.math.niveauMultiplication, tests.math.niveauMultiplication - 3);
        monterDifficulteGlobalReponseBonne(difficulte, false, question);
        assertEquals(difficulte.math.niveauMultiplication, tests.math.niveauMultiplication + 1);

        question.type = "2";
        question.genreQuestion = "modelisation";
        monterDifficulteGlobalReponseBonne(difficulte, true, question);
        assertEquals(difficulte.math.niveauMultiplication, tests.math.niveauMultiplication - 3);
        monterDifficulteGlobalReponseBonne(difficulte, false, question);
        assertEquals(difficulte.math.niveauMultiplication, tests.math.niveauMultiplication + 2);

        question.type = "3";
        question.genreQuestion = "fraToAng";
        monterDifficulteGlobalReponseBonne(difficulte, true, question);
        assertEquals(difficulte.math.niveauMultiplication, tests.math.niveauMultiplication - 3);
        monterDifficulteGlobalReponseBonne(difficulte, false, question);
        assertEquals(difficulte.math.niveauMultiplication, tests.math.niveauMultiplication + 1);

        question.type = "4";
        question.genreQuestion = "culture";
        monterDifficulteGlobalReponseBonne(difficulte, true, question);
        assertEquals(difficulte.math.niveauMultiplication, tests.math.niveauMultiplication - 3);
        monterDifficulteGlobalReponseBonne(difficulte, false, question);
        assertEquals(difficulte.math.niveauMultiplication, tests.math.niveauMultiplication + 1);

    }

    /**
     * monterDifficulteGlobalReponseBonne augmente ou baisse les differentes variables liées au niveau de difficulté des questions
     *
     * @param difficulte
     * @param reponseBonne
     * @param question
     */


    void monterDifficulteGlobalReponseBonne(Difficulte difficulte, boolean reponseBonne, Question question) {

        if (reponseBonne) {
            switch (question.type) {
                case "1":
                    difficulte.math.niveauGlobal++;

                    switch (question.genreQuestion) {
                        case "multiplication":

                            if (difficulte.math.niveauMultiplication > 4) {
                                difficulte.math.niveauMultiplication = difficulte.math.niveauMultiplication - 3;
                                difficulte.math.niveauAddition = difficulte.math.niveauAddition + 1;
                                difficulte.math.niveauSoustraction = difficulte.math.niveauSoustraction + 1;
                                difficulte.math.niveauDivision = difficulte.math.niveauDivision + 1;
                            }
                            break;

                        case "addition":

                            if (difficulte.math.niveauAddition > 4) {
                                difficulte.math.niveauMultiplication = difficulte.math.niveauMultiplication + 1;
                                difficulte.math.niveauAddition = difficulte.math.niveauAddition - 3;
                                difficulte.math.niveauSoustraction = difficulte.math.niveauSoustraction + 1;
                                difficulte.math.niveauDivision = difficulte.math.niveauDivision + 1;
                            }
                            break;

                        case "soustraction":

                            if (difficulte.math.niveauSoustraction > 4) {
                                difficulte.math.niveauMultiplication = difficulte.math.niveauMultiplication + 1;
                                difficulte.math.niveauAddition = difficulte.math.niveauAddition + 1;
                                difficulte.math.niveauSoustraction = difficulte.math.niveauSoustraction - 3;
                                difficulte.math.niveauDivision = difficulte.math.niveauDivision + 1;
                            }
                            break;

                        case "division":

                            if (difficulte.math.niveauDivision > 4) {
                                difficulte.math.niveauMultiplication = difficulte.math.niveauMultiplication + 1;
                                difficulte.math.niveauAddition = difficulte.math.niveauAddition + 1;
                                difficulte.math.niveauSoustraction = difficulte.math.niveauSoustraction + 1;
                                difficulte.math.niveauDivision = difficulte.math.niveauDivision - 3;
                            }
                            break;
                    }
                    break;


                case "2":
                    difficulte.logique.niveauGlobal++;

                    switch (question.genreQuestion) {
                        case "modelisation":

                            if (difficulte.logique.niveauModelisation > 5) {
                                difficulte.logique.niveauModelisation = difficulte.logique.niveauModelisation - 3;
                                difficulte.logique.niveauConversion = difficulte.logique.niveauConversion + 3;
                            }
                            break;

                        case "conversion":

                            if (difficulte.logique.niveauConversion > 5) {
                                difficulte.logique.niveauModelisation = difficulte.logique.niveauModelisation + 3;
                                difficulte.logique.niveauConversion = difficulte.logique.niveauConversion - 3;
                            }
                            break;

                    }
                    break;


                case "3":
                    difficulte.anglais.niveauGlobal++;

                    switch (question.genreQuestion) {
                        case "fraToAng":

                            if (difficulte.anglais.niveauFraToAng > 5) {
                                difficulte.anglais.niveauFraToAng = difficulte.anglais.niveauFraToAng - 4;
                                difficulte.anglais.niveauAngToFra = difficulte.anglais.niveauAngToFra + 2;
                                difficulte.anglais.niveauCultureAng = difficulte.anglais.niveauCultureAng + 2;
                            }
                            break;

                        case "angToFra":

                            if (difficulte.anglais.niveauAngToFra > 5) {
                                difficulte.anglais.niveauFraToAng = difficulte.anglais.niveauFraToAng + 2;
                                difficulte.anglais.niveauAngToFra = difficulte.anglais.niveauAngToFra - 4;
                                difficulte.anglais.niveauCultureAng = difficulte.anglais.niveauCultureAng + 2;
                            }
                            break;

                        case "cultureAng":

                            if (difficulte.anglais.niveauCultureAng > 5) {
                                difficulte.anglais.niveauFraToAng = difficulte.anglais.niveauFraToAng + 2;
                                difficulte.anglais.niveauAngToFra = difficulte.anglais.niveauAngToFra + 2;
                                difficulte.anglais.niveauCultureAng = difficulte.anglais.niveauCultureAng - 4;
                            }
                            break;
                    }
                    break;


                case "4":
                    difficulte.histoire.niveauGlobal++;

                    switch (question.genreQuestion) {
                        case "date":

                            if (difficulte.histoire.niveauDate > 5) {
                                difficulte.histoire.niveauDate = difficulte.histoire.niveauDate - 4;
                                difficulte.histoire.niveauCulture = difficulte.histoire.niveauCulture + 2;
                                difficulte.histoire.niveauGeographie = difficulte.histoire.niveauGeographie + 2;
                            }
                            break;

                        case "culture":

                            if (difficulte.histoire.niveauCulture > 5) {
                                difficulte.histoire.niveauDate = difficulte.histoire.niveauDate + 2;
                                difficulte.histoire.niveauCulture = difficulte.histoire.niveauCulture - 4;
                                difficulte.histoire.niveauGeographie = difficulte.histoire.niveauGeographie + 2;
                            }
                            break;

                        case "geographie":

                            if (difficulte.histoire.niveauGeographie > 5) {
                                difficulte.histoire.niveauDate = difficulte.histoire.niveauDate + 2;
                                difficulte.histoire.niveauCulture = difficulte.histoire.niveauCulture + 2;
                                difficulte.histoire.niveauGeographie = difficulte.histoire.niveauGeographie - 4;
                            }
                            break;
                    }
                    break;
            }
        } else {
            switch (question.type) {
                case "1":
                    difficulte.math.niveauGlobal--;
                    break;

                case "2":
                    difficulte.logique.niveauGlobal--;
                    break;

                case "3":
                    difficulte.anglais.niveauGlobal--;
                    break;

                case "4":
                    difficulte.histoire.niveauGlobal--;
                    break;

            }
        }
    }

    /**
     * TODO Définir l'aglo pour définir le niveau du monstre
     * quelNiveau retourne le niveau du monstre
     *
     * @return niveau du monstre
     */
    String quelNiveau(String type, Difficulte difficulte) {
        switch (type) {
            case "1":
                if (difficulte.math.niveauGlobal < 10) {
                    return "1";
                } else if (difficulte.math.niveauGlobal < 20) {
                    return "2";
                } else if (difficulte.math.niveauGlobal < 30) {
                    return "3";
                } else {
                    return "4";
                }

            case "2":

                if (difficulte.logique.niveauGlobal < 10) {
                    return "1";
                } else if (difficulte.logique.niveauGlobal < 20) {
                    return "2";
                } else if (difficulte.logique.niveauGlobal < 30) {
                    return "3";
                } else {
                    return "4";
                }

            case "3":
                if (difficulte.anglais.niveauGlobal < 10) {
                    return "1";
                } else if (difficulte.anglais.niveauGlobal < 20) {
                    return "2";
                } else if (difficulte.anglais.niveauGlobal < 30) {
                    return "3";
                } else {
                    return "4";
                }
            case "4":

                if (difficulte.histoire.niveauGlobal < 10) {
                    return "1";
                } else if (difficulte.histoire.niveauGlobal < 20) {
                    return "2";
                } else if (difficulte.histoire.niveauGlobal < 30) {
                    return "3";
                } else {
                    return "4";
                }

            default:
                return "";
        }
    }

    /**
     * initMonstre initialise l'objet monstre en paramètre avec :
     * - son niveau
     * - ses pointDeVie
     * - sa couleur
     * - matierePointFaible
     *
     * @param monstre            : l'objet à initialiser
     * @param niveau             : le niveau du monstre
     *                           pointDeVie du monstre en fonction de son niveau
     * @param matierePointFaible : la matière point faible du monstre : sa couleur
     */

    void initMonstre(Monstre monstre, int niveau, String matierePointFaible) {
        monstre.niveau = niveau;
        monstre.pointDeVie = 2000 + 200 * niveau;

        if (equals(matierePointFaible, "alea")) {
            monstre.matierePointFaible = "" + (int) (random() * 4 + 1);
        } else {
            monstre.matierePointFaible = matierePointFaible;
        }
    }

    void testInitMonstre() {
        Monstre testMonstre = new Monstre();

        initMonstre(testMonstre, 1, "MATH");
        assertEquals(testMonstre.niveau, 1);
        assertEquals(testMonstre.pointDeVie, 2200);
        assertEquals(testMonstre.matierePointFaible, "MATH");

        initMonstre(testMonstre, 4, "HISTOIRE");
        assertEquals(testMonstre.niveau, 4);
        assertEquals(testMonstre.pointDeVie, 2800);
        assertEquals(testMonstre.matierePointFaible, "HISTOIRE");
    }


    /**
     * mettreAJourEtatMonstre met à jour l'état graphique du monstre (normal / attaque / regen) en fonction de la réponse du
     * joueur
     *
     * @param question la question contenant la réponse
     * @param reponse  la réponse du joueur
     * @return un état
     * @see Etat
     */

    int mettreAJourEtatMonstre(Question question, String reponse) {
        if (reponseBonne(question, reponse)) {
            return ETAT_MONSTRE.ATTAQUE;
        } else {
            return ETAT_MONSTRE.REGEN;
        }
    }

    void testMettreAJourEtatMonstre() {
        Question testQuestion = new Question();
        testQuestion.reponse = "bonjour";

        assertEquals(mettreAJourEtatMonstre(testQuestion, "bonjour"), 2);
        assertEquals(mettreAJourEtatMonstre(testQuestion, "nein nein nein"), 1);

    }

    /**
     * Focntion test
     */

    void testParseQuestion() {
        for (int type = 1; type <= 4; type++) {
            Question[] question = getAllQuestionOfOneMatiere(fichierQuestion, "" + type, "multiplication");

            for (int i = 0; i < length(question); i++) {
                parseQuestion(question[i]);

                println("Décomposition de l'objet Question");
                println("id : " + question[i].id);
                println("question : " + question[i].question);
                println("reponse : " + question[i].reponse);
                println("reponseAffiche : " + question[i].reponseAffiche);
                println("type : " + question[i].type);
                println("niveau : " + question[i].niveau);

                String vars = "[";

                for (int idx = 0; idx < length(question[i].vars); idx++) {
                    vars += question[i].vars[idx] + ", ";
                }

                if (length(vars) > 1) {
                    vars = substring(vars, 0, length(vars) - 2) + "]";
                } else {
                    vars += "]";
                }

                println("vars : " + vars);
                println("adaptative : " + question[i].adaptative);

                println();
                println();

            }
        }
    }

    /**
     * getQuestion choisie une question dans un fichier CSV en fonction du type de question et de ça difficulté
     * type compris entre 0 et 4
     * niveau compris entre 0 et 3
     *
     * @param fichierQuestion
     * @param type
     * @param niveau
     * @return question initialisée et parsée si adaptative
     */

    Question getQuestion(CSVFile fichierQuestion, String type, String niveau, String genre) {
        Question question = aleaQuestion(fichierQuestion, type, niveau, genre);

        if (question.adaptative) {
            parseQuestion(question);
        }

        return question;
    }

    /*======================================================================================*/
    /*
      /*  Fonctions relatives à l'interprétation du language permettant de générer les
      /*  questions adaptatives :
      /*  - le code interprété est écrit dans les questions, les réponses, et les réponses à
      /*    afficher à l'utilisateurs toujours entre crochet {}
      /*  - une fonction commence par son nom suivie de parenthèse entourant ses paramètres si
      /*    ils sont nécessaires eux même séparré par une virgule
      /*  - les variables lorsqu'elles sont affichées seules peuvent être indiquée entre
      /*    crochets
      /*
      /*
      /*  - les paramètres d'une fonction sont donnés entre parenthèses
      /*  - toute fonction passé en paramètres d'une autre fonction doit être entourée de
      /*    crochets {}
      /*  - la case est importante
      /*  - les espaces ne sont pas importants
      /*
      /*======================================================================================*/

    /**
     * parseQuestion assigne à l'objet question :
     * - sa question
     * - ses varibles (comprises dans la question)
     * - la réponse à sa question (uniquement le mot ou le chiffre que doit entrer le joueur)
     * - la réponse à affiicher (plus détaillé que la réponse que doit entrer le joueur)
     *
     * @param question la question à analyser
     */

    void parseQuestion(Question question) {
        question.question = parseString(question.question, true, question.vars);
        question.vars = reductionTableau(question.vars);
        question.reponse = parseString(question.reponse, false, question.vars);
        question.reponseAffiche = parseString(question.reponseAffiche, false, question.vars);
    }

    /**
     * parseString analyse la question
     *
     * @param chaine
     * @param addToVars
     * @param vars
     * @return
     */

    String parseString(String chaine, boolean addToVars, String[] vars) {
        String[] sousChaines = searchInString(chaine);
        return parseTab(sousChaines, addToVars, vars);
    }

    /**
     * searchInString cherche dans une chaine le code
     *
     * @param chaine
     * @return
     */

    String[] searchInString(String chaine) {
        // On suppose que chaine ne contient pas plus de 10 sous chaines
        String[] sousChaines = new String[10];

        //initialisation tableau de String (null => "")

        int idxTab = 0;
        int dimension = 0;
        boolean estDansDimension = false;
        int idxString = 0;


        while (idxString < length(chaine)) {

            if (charAt(chaine, idxString) == '{') {
                dimension++;
            }

            if (!estDansDimension && dimension > 0) {
                idxTab++;
                estDansDimension = true;
            } else if (estDansDimension && dimension == 0) {
                idxTab++;
                estDansDimension = false;
            }

            sousChaines[idxTab] = nonNull(sousChaines[idxTab]);
            sousChaines[idxTab] += "" + charAt(chaine, idxString);

            if (charAt(chaine, idxString) == '}') {
                dimension--;
            }

            idxString++;
        }

        return reductionTableau(sousChaines);
    }

    /**
     * parseTab analyse un tableau contenant dans chaine de caractères simple et du code
     *
     * @param sousChaines
     * @param addToVars
     * @param vars
     * @return une unique chaine de caractères avec les fonctions remplacées par leur résultat
     */

    String parseTab(String[] sousChaines, boolean addToVars, String[] vars) {
        int idxvars = 0;

        for (int i = 0; i < length(sousChaines); i++) {
            if (charAt(sousChaines[i], 0) == '{') {
                sousChaines[i] = getFonctionResult(replace(substring(sousChaines[i], 1, length(sousChaines[i]) - 1), " ", "", -1), vars);
                if (addToVars) {
                    vars[idxvars] = sousChaines[i];
                    idxvars++;
                }
            }
        }

        return stringTabToString(sousChaines);
    }


    /**
     * stringTabToString transforme un tableau de chaine en une chaine
     *
     * @param tab
     * @return une chaine de caractères (concaténation du tableau en paramètre)
     */
    String stringTabToString(String[] tab) {
        String concat = "";
        for (int i = 0; i < length(tab); i++) {
            concat += tab[i];
        }
        return concat;
    }


    void testGetFonctionResult() {
        String[] vars = {};

        assertEquals(getFonctionResult("couleur(red)", vars), "rouge");
        println(getFonctionResult("couleur(fr)", vars));
        println(getFonctionResult("couleur(en)", vars));
        println(getFonctionResult("divide(10, 10)", vars));
        println(getFonctionResult("divide(5, 10)", vars));
    }


    /**
     * getFonctionResult interpréte la fonction qui lui passé paramètre
     *
     * @param chaine ex: randInt(1,10)
     * @param vars
     * @return
     */
    String getFonctionResult(String chaine, String[] vars) {
        String[] param = new String[10];

        nonNull(param);

        getParam(chaine, param);

        param = reductionTableau(param);

        parseParam(param, vars);

        for (int arg = 0; arg < length(param); arg++) {
            if (charAt(chaine, 0) == '{') {
                parseString(param[0], false, vars);
            }
        }

        switch (getFonctionName(chaine)) {
            case "randInt":
                return "" + (int) (stringToInt(param[0]) + random() * stringToInt(param[1]));
            case "multiply":
                return "" + stringToInt(param[0]) * stringToInt(param[1]);
            case "add":
                return (stringToInt(param[0]) + stringToInt(param[1])) + "";
            case "divide":
                double resultat = stringToDouble(param[0]) / stringToDouble(param[1]);
                //precision simple
                resultat = ((int) (resultat * 100)) / 100.0;

                if (resultat * 100 == ((int) resultat) * 100) {
                    return ((int) resultat) + "";
                } else {
                    return replace(resultat + "", ".", ",", 1);
                }


                //return replace((stringToDouble(param[0]) / stringToDouble(param[1])) + "", ".", ",", 1);
            case "minus":
                return (stringToInt(param[0]) - stringToInt(param[1])) + "";
            case "print":
                return param[0];
            case "couleur":
                String couleur;

                if (equals(param[0], "fr")) {
                    String[] couleursFr = {"rouge", "vert", "bleu"};
                    couleur = couleursFr[(int) (random() * length(couleursFr))];

                } else if (equals(param[0], "en")) {
                    String[] couleursEn = {"red", "green", "blue"};
                    couleur = couleursEn[(int) (random() * length(couleursEn))];

                } else {
                    String[] couleursEn = {"red", "green", "blue"};
                    String[] couleursFr = {"rouge", "vert", "bleu"};
                    int idx = searchInTab(param[0], couleursEn);

                    if (idx == -1) {
                        idx = searchInTab(param[0], couleursFr);
                        couleur = couleursEn[idx];
                    } else {
                        couleur = couleursFr[idx];
                    }

                }

                return couleur;
            case "traduction":

            default:
                return chaine;
        }
    }


    void testSearchInTab() {
        String[] tab = {"red", "green", "blue"};

        assertEquals(searchInTab("red", tab), 0);
    }


    /**
     * recherche une valeur dans un tableau
     *
     * @param val la valeur à chercher
     * @param tab le tableau de valeurs
     * @return l'indice de la valeur
     */

    int searchInTab(String val, String[] tab) {
        int i = 0;

        while (i < length(tab) && !equals(tab[i], val)) {
            i++;
        }

        if (length(tab) == i) {
            return -1;
        } else {
            return i;
        }
    }


    /**
     * replace permet de substituer un caractère dans une chaine de caractères pour un nombre de fois défini
     *
     * @param chaine              est la chaine contant le caractère à remplacer
     * @param replace             est le caractère à remplacer
     * @param to                  est le caractère ajouter à la place
     * @param numberOfReplacement est compris entre -1 et +inf
     * @return la nouvelle chaine de caractères
     */
    String replace(String chaine, String replace, String to, int numberOfReplacement) {
        String newString = "";
        int replaceCount = 0;

        for (int i = 0; i < length(chaine); i++) {
            if (equals(charAt(chaine, i) + "", replace) && (replaceCount < numberOfReplacement || numberOfReplacement == -1)) {
                newString += to;
                replaceCount++;
            } else {
                newString += "" + charAt(chaine, i);
            }
        }
        return newString;
    }

    /**
     * parseParam permet d'interpréter les paramètres d'une fonction (cas d'une variable ou sous fonction)
     *
     * @param param est le tableau contenant les paramètres
     * @param vars  est le tableau contenant les variables éventuelles
     */
    void parseParam(String[] param, String[] vars) {
        for (int i = 0; i < length(param); i++) {
            if (param[i] != null && length(param[i]) > 3 && charAt(param[i], 0) == 'v' && charAt(param[i], 1) == 'a' && charAt(param[i], 2) == 'r') {
                param[i] = vars[charToInt(charAt(param[i], 3))];
            } else if (param[i] != null && length(param[i]) > 1 && charAt(param[i], 0) == '{') {
                param[i] = parseString(param[i], false, vars);
            }
        }
    }

    /**
     * getFonctionName permet d'obtenir le nom de la fonction interprétée
     *
     * @param chaine est la chaine contenant le nom de la fonction
     * @return le nom de la fonction
     */
    String getFonctionName(String chaine) {
        int i = 0;
        String name = "";

        while (i < length(chaine) && charAt(chaine, i) != '(') {
            name += "" + charAt(chaine, i);
            i++;
        }

        return name;
    }


    /**
     * Fonction de test
     */
    void testGetParam() {
        String[] param = new String[2];
        String[] expectedParam = {"{add({add(var0,{add(var0,var2)})},var2)}", "var1"};
        nonNull(param);

        getParam("minus({add({add(var0,{add(var0,var2)})},var2)},var1)", param);
        assertArrayEquals(param, expectedParam);
    }


    /**
     * getParam permet d'obtenir les paramètre de la fonction interprétée
     *
     * @param chaine est la chaine de caractères contant les paramètres
     * @param param  est le tableau qui contiendra les paramètres de la fonction
     *               si aucun paramètre n'est trouvé avec param[0] = chaine
     */
    void getParam(String chaine, String[] param) {
        int idxParam = 0;
        int dimension = 0;
        int chaineIdx = 0;
        char c;

        while (length(chaine) > chaineIdx && charAt(chaine, chaineIdx) != '(') {
            chaineIdx++;
        }

        if (length(chaine) > chaineIdx) {
            do {
                chaineIdx++;
                // question pratique
                c = charAt(chaine, chaineIdx);

                if (dimension == 0 && c == ',') {
                    idxParam++;
                }

                if (c == '{') {
                    dimension++;
                }

                if (!(dimension == 0 && (c == ',' | c == ')'))) {
                    param[idxParam] += "" + c;
                }

                if (c == '}') {
                    dimension--;
                }


            } while ((charAt(chaine, chaineIdx) != ')' || dimension != 0) || charAt(chaine, chaineIdx) == '}');
        } else {
            param[0] = chaine;
        }
    }


    /**
     * Fonction de test
     */
    void testSearchInString() {
        String chaine = "Combien font {randInt(1,100)} kilomètres en mètres ?";
        String[] sousChaine = {"Combien font ", "{randInt(1,100)}", " kilomètres en mètres ?"};
        assertArrayEquals(searchInString(chaine), sousChaine);
    }


    String nonNull(String chaine) {
        if (chaine == null) {
            return "";
        }
        return chaine;
    }

    /**
     * nonNull initialise un tableau de chaine de caractères avec ""
     *
     * @param chaine est le tableau à initialiser
     */
    void nonNull(String chaine[]) {
        for (int i = 0; i < length(chaine); i++) {
            if (chaine[i] == null) {
                chaine[i] = "";
            }
        }
    }

    /**
     * calculeProbaQuestionAdaptatif permet de savoir quel est la question aleatoire choisie
     *
     * @param difficulte
     * @param type
     */

    String calculeProbaQuestionAdaptatif(Difficulte difficulte, String type) {
        int randomInt = (int) (random() * 100);
        switch (type) {
            case "1":
                if (randomInt < difficulte.math.niveauMultiplication) {
                    return "multiplication";
                } else if (randomInt < difficulte.math.niveauMultiplication + difficulte.math.niveauAddition) {
                    return "addition";
                } else if (randomInt < difficulte.math.niveauMultiplication + difficulte.math.niveauAddition + difficulte.math.niveauSoustraction) {
                    return "soustraction";
                } else {
                    return "division";
                }

            case "2":
                if (randomInt < difficulte.logique.niveauModelisation) {
                    return "modelisation";
                } else {
                    return "conversion";
                }

            case "3":
                if (randomInt < difficulte.anglais.niveauFraToAng) {
                    return "fraToAng";
                } else if (randomInt < difficulte.anglais.niveauFraToAng + difficulte.anglais.niveauAngToFra) {
                    return "angToFra";
                } else {
                    return "cultureAng";
                }

            case "4":
                if (randomInt < difficulte.histoire.niveauDate) {
                    return "date";
                } else if (randomInt < difficulte.histoire.niveauDate + difficulte.histoire.niveauCulture) {
                    return "culture";
                } else {
                    return "geographie";
                }

            default:
                return "";
        }
    }

    /**
     * aleaQuestion choisie une question compatible aléatoirement
     *
     * @param fichierQuestion est le fichier contenant les questions
     * @param type            est le type de la question demandée (matière)
     * @param niveau          est le niveau de la question demandé
     * @return une question compatible avec les paramètres
     */
    Question aleaQuestion(CSVFile fichierQuestion, String type, String niveau, String genre) {
        Question[] questionCompatible = new Question[rowCount(fichierQuestion) - 1]; // -1 pour la ligne d'entête du fichier CSV

        //Réasignation du tableau pour obtenir en taille le nombre réelle de questions compatibles
        questionCompatible = getQuestionCompatible(questionCompatible, type, niveau, genre);

        // Choisie une question de manière aléatoire dans les questions compatibles
        return questionCompatible[(int) (random() * length(questionCompatible))];
    }


    Question[] getAllQuestionOfOneMatiere(CSVFile fichierQuestion, String type, String genre) {
        Question[] questionCompatible = new Question[rowCount(fichierQuestion) - 1]; // -1 pour la ligne d'entête du fichier CSV

        //Réasignation du tableau pour obtenir en taille le nombre réelle de questions compatibles
        questionCompatible = getQuestionCompatible(questionCompatible, type, "any", genre);

        return questionCompatible;
    }

    /**
     * getCompatibleQuestions parcour un fichier CSV pour trouver les questions compatibles avec le type et le niveau passé en paramètre
     *
     * @param questionCompatible
     * @param type
     * @param niveau
     * @return Question[]
     */
    Question[] getQuestionCompatible(Question[] questionCompatible, String type, String niveau, String genre) {
        int idxAppend = 0;

        for (int i = 1; i < rowCount(fichierQuestion); i++) {
            if (equals(getCell(fichierQuestion, i, 1), type) &&
                    equals(getCell(fichierQuestion, i, 7), genre) &&
                    (equals(getCell(fichierQuestion, i, 3), niveau) ||
                            equals(niveau, "any"))) {
                questionCompatible[idxAppend] = initQuestion(fichierQuestion, i);
                idxAppend++;
            }
        }

        // Réduit la taille du tableau des questions compatibles au nombre de question trouvées (TR = taille réelle)
        return reductionTableau(questionCompatible);
    }

    /**
     * initQuestion initialise une question en fonction d'une ligne dans le fichier question
     *
     * @param fichierQuestion est le fichier contenant les questions
     * @param line            est la line de la question dans le csv
     * @return une question initialisé
     */
    Question initQuestion(CSVFile fichierQuestion, int line) {
        Question question = new Question();
        question.id = getCell(fichierQuestion, line, 0);
        question.type = getCell(fichierQuestion, line, 1);
        question.question = getCell(fichierQuestion, line, 2);
        question.niveau = getCell(fichierQuestion, line, 3);
        question.reponse = getCell(fichierQuestion, line, 4);
        question.reponseAffiche = getCell(fichierQuestion, line, 5);
        question.adaptative = estAdaptative(getCell(fichierQuestion, line, 6));
        question.genreQuestion = getCell(fichierQuestion, line, 7);

        return question;
    }

    /**
     * estAdaptative vérifie si la question est adatative est renvoie un boolean en fonction
     *
     * @param adapt chaine à vérifier
     * @return boolean
     */
    boolean estAdaptative(String adapt) {
        if (equals(adapt, "TRUE")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * reductionTableau réduit un tableau : supprime les cases null
     *
     * @param tab est le tableau à réduire
     * @return un nouveau tableau sans cases null
     */
    Question[] reductionTableau(Question[] tab) {
        int taille = 0;
        for (int i = 0; i < length(tab); i++) {
            if (tab[i] != null) {
                taille++;
            }
        }

        Question[] newTab = new Question[taille];
        for (int i = 0; i < taille; i++) {
            if (tab[i] != null) {
                newTab[i] = tab[i];
            }
        }
        return newTab;
    }


    /**
     * reductionTableau réduit un tableau : supprime les cases null
     *
     * @param tab est le tableau à réduire
     * @return un nouveau tableau sans cases null
     */
    String[] reductionTableau(String[] tab) {
        int taille = 0;
        for (int i = 0; i < length(tab); i++) {
            if (tab[i] != null && tab[i] != "") {
                taille++;
            }
        }

        String[] newTab = new String[taille];
        int newTabIdx = 0;

        for (int i = 0; i < length(tab); i++) {
            if (tab[i] != null && tab[i] != "") {
                newTab[newTabIdx] = tab[i];
                newTabIdx++;
            }
        }
        return newTab;
    }

    /**
     * initialiserCarte initialise les Monstres sur la cartes, génère les anciens monstres non tué et en génère de nouveaux.
     * si début de la partie initialise des monstres niveau 0 ce qui signifie pas de monstre
     */

    void initMonstreSurLaCarte(Monstre[] monstresSurLaCarte, int nbJour) {
        if (nbJour == 0) {
            monstresSurLaCarte[0] = new Monstre();
            initMonstre(monstresSurLaCarte[0], 1, "alea");
            for (int i = 1; i < length(monstresSurLaCarte); i++) {
                monstresSurLaCarte[i] = new Monstre();
                initMonstre(monstresSurLaCarte[i], 0, "alea");
            }
        } else {
            levelUpMonstresSurLaCarte(monstresSurLaCarte);
            if (countMonstresSurLaCarte(monstresSurLaCarte) != 8) {
                int nbAlea = (int) (random() * 8);
                while (monstresSurLaCarte[nbAlea].niveau != 0) {
                    nbAlea = (int) (random() * 8);
                }
                monstresSurLaCarte[nbAlea] = new Monstre();
                initMonstre(monstresSurLaCarte[nbAlea], 1, "alea");
            }
        }
    }

    void testInitMonstreSurLaCarte() {

        Monstre[] testMonstresSurLaCarte = new Monstre[8];

        initMonstreSurLaCarte(testMonstresSurLaCarte, 0);

        //testPremière génération
        assertEquals(testMonstresSurLaCarte[0].niveau, 1);
        assertEquals(testMonstresSurLaCarte[1].niveau, 0);
        assertEquals(testMonstresSurLaCarte[4].niveau, 0);
        assertEquals(testMonstresSurLaCarte[7].niveau, 0);


        initMonstreSurLaCarte(testMonstresSurLaCarte, 1);

        //testLevelUp
        assertEquals(testMonstresSurLaCarte[0].niveau, 2);

        //test qu'un seul autre monstre a été créé
        int test = 0;
        for (int i = 1; i < length(testMonstresSurLaCarte); i++) {
            if (testMonstresSurLaCarte[i].niveau == 1) {
                test++;
            }
        }
        assertEquals(test, 1);

    }


    /**
     * countMonstresSurLaCarte compte les Monstres sur la cartes
     *
     * @param monstresSurLaCarte est le tableau de Monstre qui répertorie les monstres
     * @return le nombre de monstre présent sur la carte
     */

    int countMonstresSurLaCarte(Monstre[] monstresSurLaCarte) {
        int nbMonstresSurLaCarte = 0;
        for (int i = 0; i < length(monstresSurLaCarte); i++) {
            if (monstresSurLaCarte[i].niveau != 0) {
                nbMonstresSurLaCarte++;
            }
        }
        return nbMonstresSurLaCarte;
    }

    void testCountMonstresSurLaCarte() {

        Monstre[] testMonstresSurLaCarte = new Monstre[8];

        initMonstreSurLaCarte(testMonstresSurLaCarte, 0);
        assertEquals(countMonstresSurLaCarte(testMonstresSurLaCarte), 1);

        initMonstreSurLaCarte(testMonstresSurLaCarte, 1);
        initMonstreSurLaCarte(testMonstresSurLaCarte, 1);
        initMonstreSurLaCarte(testMonstresSurLaCarte, 1);
        assertEquals(countMonstresSurLaCarte(testMonstresSurLaCarte), 4);

    }

    /**
     * levelUpMonstresSurLaCarte monte le niveau des monstres sur la carte
     */
    void levelUpMonstresSurLaCarte(Monstre[] monstresSurLaCarte) {
        for (int i = 0; i < length(monstresSurLaCarte); i++) {
            if (monstresSurLaCarte[i].niveau != 0) {
                initMonstre(monstresSurLaCarte[i], monstresSurLaCarte[i].niveau + 1, monstresSurLaCarte[i].matierePointFaible);
            }
        }
    }

    void testLevelUpMonstresSurLaCarte() {
        Monstre[] testMonstresSurLaCarte = new Monstre[8];
        initMonstreSurLaCarte(testMonstresSurLaCarte, 0);

        levelUpMonstresSurLaCarte(testMonstresSurLaCarte);
        assertEquals(testMonstresSurLaCarte[0].niveau, 2);


    }

    /**
     * ChoixDurantCarte attend le choix de l'utilisateur pour lancer un combat contre un monstre ou quitter
     */

    char choixDurantCarte() {
        String choixUtilisateur = "";
        System.out.println("Quel est ton choix ?");

        while (!(length(choixUtilisateur) == 1 && ((charAt(choixUtilisateur, 0) >= '1' && charAt(choixUtilisateur, 0) <= '8') || equals(choixUtilisateur, "q")))) {
            choixUtilisateur = readString();
        }

        switch (choixUtilisateur) {
            case "1":
                return '1';
            case "2":
                return '2';
            case "3":
                return '3';
            case "4":
                return '4';
            case "5":
                return '5';
            case "6":
                return '6';
            case "7":
                return '7';
            case "8":
                return '8';
            case "q":
                return 'q';
            default:
                return 'z'; // ne peut pas être retourné
        }
    }

    /**
     * algorithme principale
     */

    void algorithm() {

        boolean finProgramme = false;

        //Boucle programme

        while (!finProgramme) {
            clearScreen();
            afficherMenu();

            switch (readString()) {
                case "1":
                    jouer();
                    break;
                case "2":
                    statistique();
                    break;
                case "3":
                    finProgramme = true;
                    break;
            }
        }

        println("A bientôt, jenune chevalier !");
    }

    /**
     * statistique
     */

    void statistique() {
        clearScreen();

        CSVFile sauvegarde = loadCSV("Sauvegarde.csv", '\t');

        Difficulte difficulte = new Difficulte();
        Monstre[] monstresSurLaCarte = new Monstre[8];

        int nbJour = 0;

        initMonstreSurLaCarte(monstresSurLaCarte, nbJour);

        String pseudo = equals(getCell(sauvegarde, 0, 1), "NA##") ? getPrenom() : getCell(sauvegarde, 0, 1);

        nbJour = stringToInt(getCell(sauvegarde, 1, 1));

        int idxDebutDifficulte = 2;
        int monstresTues = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;

        difficulte.math.niveauGlobal = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.math.niveauMultiplication = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.math.niveauAddition = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.math.niveauSoustraction = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.math.niveauDivision = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;

        difficulte.logique.niveauGlobal = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.logique.niveauModelisation = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.logique.niveauConversion = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;

        difficulte.anglais.niveauGlobal = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.anglais.niveauFraToAng = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.anglais.niveauAngToFra = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.anglais.niveauCultureAng = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;

        difficulte.histoire.niveauGlobal = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.histoire.niveauDate = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.histoire.niveauCulture = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.histoire.niveauGeographie = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));


        int idxDebutMonstre = 19;
        for (int monstre = 0; monstre < 8; monstre++) {
            monstresSurLaCarte[monstre].niveau = stringToInt(getCell(sauvegarde, idxDebutMonstre, 1));
            monstresSurLaCarte[monstre].pointDeVie = stringToInt(getCell(sauvegarde, idxDebutMonstre + 1, 1));
            monstresSurLaCarte[monstre].matierePointFaible = getCell(sauvegarde, idxDebutMonstre + 2, 1);
            idxDebutMonstre = idxDebutMonstre + 3;

        }

        afficherStatistique(difficulte, nbJour, monstresTues, pseudo);
        readString();
    }

    /**
     * jouer
     */

    void jouer() {
        clearScreen();

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////
        // Récupération des variables stockées dans le fichier Sauvegardes.csv
        CSVFile sauvegarde = loadCSV("Sauvegarde.csv", '\t');

        Question question;
        Difficulte difficulte = new Difficulte();
        Monstre[] monstresSurLaCarte = new Monstre[8];


        String pseudo = equals(getCell(sauvegarde, 0, 1), "NA##") ? getPrenom() : getCell(sauvegarde, 0, 1);
        int nbJour = 0;

        // Initialisation de monstresSurLaCarte
        initMonstreSurLaCarte(monstresSurLaCarte, nbJour);

        nbJour = stringToInt(getCell(sauvegarde, 1, 1));
        int idxDebutDifficulte = 2;
        int monstresTues = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;

        difficulte.math.niveauGlobal = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.math.niveauMultiplication = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.math.niveauAddition = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.math.niveauSoustraction = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.math.niveauDivision = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;

        difficulte.logique.niveauGlobal = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.logique.niveauModelisation = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.logique.niveauConversion = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;

        difficulte.anglais.niveauGlobal = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.anglais.niveauFraToAng = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.anglais.niveauAngToFra = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.anglais.niveauCultureAng = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;

        difficulte.histoire.niveauGlobal = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.histoire.niveauDate = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.histoire.niveauCulture = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        idxDebutDifficulte++;
        difficulte.histoire.niveauGeographie = stringToInt(getCell(sauvegarde, idxDebutDifficulte, 1));
        int idxDebutMonstre = 19;
        for (int monstre = 0; monstre < 8; monstre++) {
            monstresSurLaCarte[monstre].niveau = stringToInt(getCell(sauvegarde, idxDebutMonstre, 1));
            monstresSurLaCarte[monstre].pointDeVie = stringToInt(getCell(sauvegarde, idxDebutMonstre + 1, 1));
            monstresSurLaCarte[monstre].matierePointFaible = getCell(sauvegarde, idxDebutMonstre + 2, 1);
            idxDebutMonstre = idxDebutMonstre + 3;
        }
        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        // Définition des variables de fonctionnent de l'algo
        int etatPrecedant = ETAT_MONSTRE.NORMAL;
        boolean perdu = false;
        boolean quitter = false;
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////


        clearScreen();


        // Boucle en partie
        while (!perdu && !quitter) {
            nbJour++;

            // Upgrade les monstres sur la carte et en ajoute un lorsqu'un autre est tué
            initMonstreSurLaCarte(monstresSurLaCarte, nbJour);
            afficherCarte(nbJour, monstresSurLaCarte);
            int choixDurantCarte = (int) choixDurantCarte() - '1';

            // Termine le jeu si le joueur à appuyé sur q
            if (choixDurantCarte == 'q' - '1') {

                // Tableau des variables à sauvegarder
                String[][] tableauSauvegarde =
                        {
                                {"pseudo", pseudo},

                                {"nbJour", nbJour + ""},

                                {"monstresTues", monstresTues + ""},

                                {"Math.niveauGlobal", difficulte.math.niveauGlobal + ""},

                                {"Math.niveauMultiplication", difficulte.math.niveauMultiplication + ""},

                                {"Math.niveauAddition", difficulte.math.niveauAddition + ""},

                                {"Math.niveauSoustraction", difficulte.math.niveauSoustraction + ""},

                                {"Math.niveauDivision", difficulte.math.niveauDivision + ""},

                                {"Logique.niveauGlobal", difficulte.logique.niveauGlobal + ""},

                                {"Logique.niveauModelisation", difficulte.logique.niveauModelisation + ""},

                                {"Logique.niveauConversion", difficulte.logique.niveauConversion + ""},

                                {"Anglais.niveauGlobal", difficulte.anglais.niveauGlobal + ""},

                                {"Anglais.niveauFraToAng", difficulte.anglais.niveauFraToAng + ""},

                                {"Anglais.niveauAngToFra", difficulte.anglais.niveauAngToFra + ""},

                                {"Anglais.niveauCultureAng", difficulte.anglais.niveauCultureAng + ""},

                                {"Histoire.niveauGlobal", difficulte.histoire.niveauGlobal + ""},

                                {"Histoire.niveauDate", difficulte.histoire.niveauDate + ""},

                                {"Histoire.niveauCulture", difficulte.histoire.niveauCulture + ""},

                                {"Histoire.niveauGeographie", difficulte.histoire.niveauGeographie + ""},

                                {"Monstre0.niveau", monstresSurLaCarte[0].niveau + ""},

                                {"Monstre0.pointDeVie", monstresSurLaCarte[0].pointDeVie + ""},

                                {"Monstre0.matierePointFaible", monstresSurLaCarte[0].matierePointFaible},

                                {"Monstre1.niveau", monstresSurLaCarte[1].niveau + ""},

                                {"Monstre1.pointDeVie", monstresSurLaCarte[1].pointDeVie + ""},

                                {"Monstre1.matierePointFaible", monstresSurLaCarte[1].matierePointFaible},

                                {"Monstre2.niveau", monstresSurLaCarte[2].niveau + ""},

                                {"Monstre2.pointDeVie", monstresSurLaCarte[2].pointDeVie + ""},

                                {"Monstre2.matierePointFaible", monstresSurLaCarte[2].matierePointFaible},

                                {"Monstre3.niveau", monstresSurLaCarte[3].niveau + ""},

                                {"Monstre3.pointDeVie", monstresSurLaCarte[3].pointDeVie + ""},

                                {"Monstre3.matierePointFaible", monstresSurLaCarte[3].matierePointFaible},

                                {"Monstre4.niveau", monstresSurLaCarte[4].niveau + ""},

                                {"Monstre4.pointDeVie", monstresSurLaCarte[4].pointDeVie + ""},

                                {"Monstre4.matierePointFaible", monstresSurLaCarte[4].matierePointFaible},

                                {"Monstre5.niveau", monstresSurLaCarte[5].niveau + ""},

                                {"Monstre5.pointDeVie", monstresSurLaCarte[5].pointDeVie + ""},

                                {"Monstre5.matierePointFaible", monstresSurLaCarte[5].matierePointFaible},

                                {"Monstre6.niveau", monstresSurLaCarte[6].niveau + ""},

                                {"Monstre6.pointDeVie", monstresSurLaCarte[6].pointDeVie + ""},

                                {"Monstre6.matierePointFaible", monstresSurLaCarte[6].matierePointFaible},

                                {"Monstre7.niveau", monstresSurLaCarte[7].niveau + ""},

                                {"Monstre7.pointDeVie", monstresSurLaCarte[7].pointDeVie + ""},

                                {"Monstre7.matierePointFaible", monstresSurLaCarte[7].matierePointFaible},
                        };
                // Sauvegarde dans le CSV
                saveCSV(tableauSauvegarde, "Sauvegarde.csv", '\t');
                quitter = true;
            } else {
                if (monstresSurLaCarte[choixDurantCarte].niveau == 0) {
                    println("Il n'y a pas de monstre ici");
                } else {
                    //boucle combat
                    while (monstreEstVivant(monstresSurLaCarte[choixDurantCarte])) {

                        afficherCombat(monstresSurLaCarte[choixDurantCarte], etatPrecedant);
                        etatPrecedant = ETAT_MONSTRE.NORMAL;
                        String type = matiereChoisie();
                        String genre = calculeProbaQuestionAdaptatif(difficulte, type);

                        question = getQuestion(fichierQuestion, type, quelNiveau(type, difficulte), genre);
                        afficherQuestion(question, monstresSurLaCarte[choixDurantCarte], etatPrecedant);
                        String reponse = readString();
                        etatPrecedant = mettreAJourEtatMonstre(question, reponse);
                        mettreAJourMonstre(question, monstresSurLaCarte[choixDurantCarte], reponseBonne(question, reponse));
                        afficherReponse(question, reponseBonne(question, reponse), monstresSurLaCarte[choixDurantCarte], etatPrecedant);
                        monterDifficulteGlobalReponseBonne(difficulte, reponseBonne(question, reponse), question);
                        etatPrecedant = mettreAJourEtatMonstre(question, reponse);

                    }
                    monstresTues++;
                    monstresSurLaCarte[choixDurantCarte].niveau = 0;
                    println();
                    println("Victoire !!!");
                }
            }

        }
    }


    /**
     * Renvoie le pseudo choisi par l'utilisateur
     * @return le pseudo
     */
     String getPrenom() {
        afficherPersonnagePrenom();
        return readString();
    }

    /**
     * Cette fonction permet d'afficher du texte en couleur avec un fond blanc si whiteBG == true
     *
     * @param texte   le texte à afficher
     * @param couleur le code couleur (ex: Ascii.BLEU)
     * @param whiteBG boolean affichage sur fond blanc
     */
    String colorString(String texte, int couleur, boolean whiteBG) {
        Ascii ascii = new Ascii();

        String codeBleu = "\u001B[38;2;29;130;200m";
        String codeRouge = "\u001B[38;2;197;31;61m";
        String codeVert = "\u001B[38;2;50;206;0m";
        String codeViolet = "\u001B[38;2;156;0;206m";
        String codeNoir = "\u001B[38;2;0;0;0m";

        String colorCode = "";

        if (whiteBG) {
            colorCode += "\u001B[48;2;255;255;255m";
        }

        if (couleur == ascii.BLEU) {
            colorCode += codeBleu;
        } else if (couleur == ascii.ROUGE) {
            colorCode += codeRouge;
        } else if (couleur == ascii.VERT) {
            colorCode += codeVert;
        } else if (couleur == ascii.VIOLET) {
            colorCode += codeViolet;
        } else {
            colorCode += codeNoir;
        }

        return colorCode + texte + "\u001B[0m";
    }

}
