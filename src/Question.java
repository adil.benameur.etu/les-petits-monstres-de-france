/**
 * Question est la classe représentant les questions posée au joueur
 */

public class Question {
    /**
     * id est l'identifiant unique de la question
     */
    String id;

    /**
     * Le type de la question est un int compris entre 0 et 4 inclus
     * 1 = Mathématiques
     * 2 = Logique
     * 3 = Anglais
     * 4 = Histoire / Géographie
     *
     * @see Matiere
     */
    String type;

    /**
     * La chaine de la question est un String afficher au joueur
     */
    String question;

    /**
     * Le niveau est un int compris déternimant la difficulté de la question
     * (nombre de niveaux encore à déterminer)
     */
    String niveau;

    /**
     * La réponse correspond à la valeur acceptée pour la question (uniquement une valeur numérique ou un mot)
     */
    String reponse;

    /**
     * La reponse affichée correspond à la réponse affichée au joueur
     */
    String reponseAffiche;

    /**
     * La question possède-t-elle des variable dans ça chaine
     */
    boolean adaptative;

    /**
     * vars contient les variables propre à la question si celle-ci est adaptative
     * Admet que la question ne compte pas plus de 100 variables
     */
    String[] vars = new String[100];

    /**
     * contient le genre de la question pour l'adaptivité
     *
     */
    String genreQuestion;
}
